package layout.pedro.demo.customtablayout

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.View
import android.widget.HorizontalScrollView
import android.widget.TextView

class CustomTabLayout : HorizontalScrollView{

    private lateinit var viewPager : ViewPager
    private lateinit var tabStrip : CustomTabStrip

    private var tabLayoutId : Int = 0
    private var textViewId : Int = 0
    private var tittleOffSet : Int = 0

    private var tabColor : Int = 0
    private var tabSelectedColor : Int = 0
    private var textColor : Int = 0
    private var textSelectedColor : Int = 0

    constructor(context : Context): super(context){ initialize(context, null)}
    constructor(context : Context, attrs : AttributeSet): super(context, attrs){ initialize(context, attrs ) }
    constructor(context : Context, attrs : AttributeSet, defStyle :Int): super(context, attrs, defStyle){

        tabStrip = CustomTabStrip(context, attrs, defStyle)

        isHorizontalScrollBarEnabled = false

        isFillViewport = true

        tittleOffSet = (24 * resources.displayMetrics.density).toInt()

        initialize(context, attrs)

        addView(tabStrip, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

    }

    fun initialize(context :Context, attrs: AttributeSet?){

    }

}